#include "MHI-AC-Ctrl-core-plus.h"
#include <vector>
#include <string>

// last updated: 20240126-11:30

static const std::vector<std::string> protection_states = {
    "Normal", "Discharge pipe temperature protection control",
    "Discharge pipe temperature anomaly", "Current safe control of inverter primary current",
    "High pressure protection control", "High pressure anomaly",
    "Low pressure protection control", "Low pressure anomaly",
    "Anti-frost prevention control", "Current cut",
    "Power transistor protection control", "Power transistor anomaly (Overheat)",
    "Compression ratio control", "-",
    "Condensation prevention control", "Current safe control of inverter secondary current",
    "Stop by compressor rotor lock", "Stop by compressor startup failure"
};

static const char* TAG = "mhi_ac_ctrl";

unsigned long room_temp_api_timeout_ms = millis(); // set in the YAML file

// 20231012 Define vanes array here to be used in the ESPhome YAML file
static const std::vector<std::string> vanesUD_texts{"Up", "Up/Center", "Center/Down", "Down", "Swing"};
static const std::vector<std::string> vanesLR_texts{"Left", "Left/Center", "Center", "Center/Right", "Right", "Wide", "Spot", "Swing"};

// enables or disables the option to heat below 18 degrees celcius.
//static const bool low_temp_heating = false;

// manual correction to bring the tsetpoint and the troom closer to each other or correct the IU sensor overstating
static byte correct_troom = 0;
static byte IU_correct_troom = 0;

class MhiAcCtrl : public climate::Climate,
                  public Component,
                  public CallbackInterface_Status {
public:
    void setup() override // runs only on startup
    {   // publishing the version of the code
        id(versionESPH) = "version: 2.8Plus 20240126";
        id(versionCORE) = "version: 2.8Plus 20240129"; //versionCoreH;
        
        // IU fan auto to enable high and medium to be set from the remote control. fan(7) set fan to auto
        mhi_ac_ctrl_core.set_fan(7); 
        this->power_ = power_off;
        this->current_temperature = NAN;
        // restore set points
        //auto restore = this->restore_state_();
        //if (restore.has_value()) {
        //    restore->apply(this);
        //} else {
            // restore from defaults
            this->mode = climate::CLIMATE_MODE_OFF;
            // initialize target temperature to some value so that it's not NAN
            this->target_temperature = roundf(clamp(this->current_temperature, this->minimum_temperature_, this->maximum_temperature_));
            this->fan_mode = climate::CLIMATE_FAN_AUTO;
            this->swing_mode = climate::CLIMATE_SWING_OFF;
        //}
        // Never send nan to HA
        if (isnan(this->target_temperature))
            this->target_temperature = 20;

        error_code_.set_icon("mdi:alert-circle");

        outdoor_temperature_.set_icon("mdi:thermometer");
        outdoor_temperature_.set_unit_of_measurement("°C");
        outdoor_temperature_.set_accuracy_decimals(2);
        outdoor_temperature_.set_device_class("temperature");
        
        return_air_temperature_.set_icon("mdi:thermometer");
        return_air_temperature_.set_unit_of_measurement("°C");
        return_air_temperature_.set_accuracy_decimals(2);
        return_air_temperature_.set_device_class("temperature");

        delta_thermostat_.set_icon("mdi:thermometer");
        delta_thermostat_.set_unit_of_measurement("°C");
        delta_thermostat_.set_accuracy_decimals(2);
        delta_thermostat_.set_device_class("temperature");
        
        iu_troom_.set_icon("mdi:thermometer");
        iu_troom_.set_unit_of_measurement("°C");
        iu_troom_.set_accuracy_decimals(2);
        iu_troom_.set_device_class("temperature");
        
        tsetpoint_offset_corrected_.set_icon("mdi:thermometer");
        tsetpoint_offset_corrected_.set_unit_of_measurement("°C");
        tsetpoint_offset_corrected_.set_accuracy_decimals(2);
        tsetpoint_offset_corrected_.set_device_class("temperature");
        
        IU_troom_correction_.set_icon("mdi:thermometer");
        IU_troom_correction_.set_unit_of_measurement("°C");
        IU_troom_correction_.set_accuracy_decimals(2);
        IU_troom_correction_.set_device_class("temperature");
        
        outdoor_unit_fan_speed_.set_icon("mdi:fan");

        indoor_unit_fan_speed_.set_icon("mdi:fan");

        compressor_frequency_.set_icon("mdi:sine-wave");
        compressor_frequency_.set_unit_of_measurement("Hz");
        compressor_frequency_.set_accuracy_decimals(1);
        compressor_frequency_.set_device_class("frequency");
        
        indoor_unit_total_run_time_.set_icon("mdi:home-clock");
//        indoor_unit_total_run_time_.set_unit_of_measurement("h");
//        indoor_unit_total_run_time_.set_device_class("duration");
        
        compressor_total_run_time_.set_icon("mdi:fan-clock");
//        compressor_total_run_time_.set_unit_of_measurement("h");
//        compressor_total_run_time_.set_device_class("duration");


        current_power_.set_icon("mdi:current-ac");
        current_power_.set_unit_of_measurement("A");
        current_power_.set_accuracy_decimals(2);
        current_power_.set_device_class("power");

        defrost_.set_icon("mdi:snowflake-melt");

        vanes_pos_.set_icon("mdi:air-filter");
        vanesUD_text_.set_icon("mdi:air-filter");
        
        vanesLR_pos_.set_icon("mdi:air-filter");
        vanesLR_text_.set_icon("mdi:air-filter");

        indoor_unit_thi_r1_.set_icon("mdi:thermometer");
        indoor_unit_thi_r1_.set_unit_of_measurement("°C");
        indoor_unit_thi_r1_.set_accuracy_decimals(2);
        indoor_unit_thi_r1_.set_device_class("temperature");
        
        indoor_unit_thi_r2_.set_icon("mdi:thermometer");
        indoor_unit_thi_r2_.set_unit_of_measurement("°C");
        indoor_unit_thi_r2_.set_accuracy_decimals(2);
        indoor_unit_thi_r2_.set_device_class("temperature");

        indoor_unit_thi_r3_.set_icon("mdi:thermometer");
        indoor_unit_thi_r3_.set_unit_of_measurement("°C");
        indoor_unit_thi_r3_.set_accuracy_decimals(2);
        indoor_unit_thi_r3_.set_device_class("temperature");

        outdoor_unit_tho_r1_.set_icon("mdi:thermometer");
        outdoor_unit_tho_r1_.set_unit_of_measurement("°C");
        outdoor_unit_tho_r1_.set_accuracy_decimals(2);
        outdoor_unit_tho_r1_.set_device_class("temperature");
        
        outdoor_unit_expansion_valve_.set_icon("mdi:valve");
        outdoor_unit_expansion_valve_.set_unit_of_measurement("pulse");
        outdoor_unit_expansion_valve_.set_accuracy_decimals(0);
        outdoor_unit_expansion_valve_.set_device_class("frequency");

        outdoor_unit_discharge_pipe_.set_icon("mdi:thermometer");
        outdoor_unit_discharge_pipe_.set_unit_of_measurement("°C");
        outdoor_unit_discharge_pipe_.set_accuracy_decimals(1);
        outdoor_unit_discharge_pipe_.set_device_class("temperature");
        
        outdoor_unit_tdsh_.set_icon("mdi:thermometer");
        outdoor_unit_tdsh_.set_unit_of_measurement("°C");
        outdoor_unit_tdsh_.set_accuracy_decimals(1);
        outdoor_unit_tdsh_.set_device_class("temperature");
        
        protection_state_.set_icon("mdi:shield-alert-outline");

        protection_state_number_.set_icon("mdi:shield-alert-outline");

        energy_used_.set_icon("mdi:lightning-bolt");
        energy_used_.set_unit_of_measurement("kWh");
        energy_used_.set_accuracy_decimals(2);
        energy_used_.set_device_class("energy");

        mhi_ac_ctrl_core.MHIAcCtrlStatus(this);
        mhi_ac_ctrl_core.init();
        mhi_ac_ctrl_core.set_frame_size(33); // default the framesize is 33 (like WF-RAC). Only 20 or 33 possible
        if ( id(extended_frame_size) == false ) {
             mhi_ac_ctrl_core.set_frame_size(20);
        }
    }

    void loop() override
    {
        if (millis() - room_temp_api_timeout_ms >= id(room_temp_api_timeout)*1000) {
            mhi_ac_ctrl_core.set_troom(0xff);  // use IU temperature sensor - 20231104 disabled in the core so it can be handled by CorePlus
            room_temp_api_timeout_ms = millis();
            ESP_LOGD("mhi_ac_ctrl", "did not receive a room_temp_api value, using IU temperature sensor");
        }

        int ret = mhi_ac_ctrl_core.loop(100); // 20231123 testing core loop 80 instead of the default 100
        if (ret < 0)
            ESP_LOGW("mhi_ac_ctrl", "mhi_ac_ctrl_core.loop error: %i", ret);
    }

    void dump_config() override                                 // for HA thermostat
    {
        LOG_CLIMATE("", "MHI-AC-Ctrl Climate", this);
        ESP_LOGCONFIG(TAG, "  Min. Temperature: %.1f°C", this->minimum_temperature_);
        ESP_LOGCONFIG(TAG, "  Max. Temperature: %.1f°C", this->maximum_temperature_);
        ESP_LOGCONFIG(TAG, "  Supports HEAT: %s", YESNO(true));
        ESP_LOGCONFIG(TAG, "  Supports COOL: %s", YESNO(true));
    }

    void cbiStatusFunction(ACStatus status, int value) override
    {
        // disabling logging of status completely because the next line is same and much more readable.
        //static int status_old = 0; // new
        //static int value_old  = 0; // new
        //static int power_old  = 0; // new
        
        static int mode_tmp = 0xff;
        // ESP_LOGD("mhi_ac_ctrl", "received status=%i value=%i power=%i", status, value, this->power_);
        
        // logging/displaying status only when one or more value(s) changes
        // if ( status_old != status || value_old != value || power_old != this->power_ ) {
        //    ESP_LOGD("mhi_ac_ctrl", "received status=%i value=%i power=%i", status, value, this->power_);
        //    status_old = status; value_old = value; power_old = this->power_; }

        //if (this->power_ == power_off) {
        //    // Workaround for status after reboot
        //    this->mode = climate::CLIMATE_MODE_OFF;
        //    this->publish_state();
        //}
        // Workaround for status after reboot 20231016
        // != is for stopping, repeated publishing of climate while being off. So not filling up the log-screen
        if (this->power_ == power_off && this->mode != climate::CLIMATE_MODE_OFF) {
            this->mode = climate::CLIMATE_MODE_OFF;
            this->publish_state();
        }

        int vanesLR_swing_value = vanesLR_swing;
        int vanesLR_sensor_value = vanesLR_pos_.state;
        int vanesUD_swing_value = vanes_swing;
        int vanesUD_sensor_value = vanes_pos_.state;
        
        switch (status) {
        case status_power:
            if (value == power_on) {
                this->power_ = power_on;
                // output_P(status, (TOPIC_POWER), PSTR(PAYLOAD_POWER_ON));
                cbiStatusFunction(status_mode, mode_tmp);
            } else {
                // output_P(status, (TOPIC_POWER), (PAYLOAD_POWER_OFF));
                // output_P(status, PSTR(TOPIC_MODE), PSTR(PAYLOAD_MODE_OFF));
                this->power_ = power_off;
                this->mode = climate::CLIMATE_MODE_OFF;
                this->publish_state();
            }
            break;
        case status_mode:
            mode_tmp = value;
        case opdata_mode:
        case erropdata_mode:
            switch (value) {
            case mode_auto:
                // if (status != erropdata_mode)
                //    output_P(status, PSTR(TOPIC_MODE), PSTR(PAYLOAD_MODE_AUTO));
                // else
                //    output_P(status, PSTR(TOPIC_MODE), PSTR(PAYLOAD_MODE_STOP));
                //    break;
                if (status != erropdata_mode && this->power_ > 0) {
                    this->mode = climate::CLIMATE_MODE_HEAT_COOL;
                } else {
                    this->mode = climate::CLIMATE_MODE_OFF;
                }
                break;
            case mode_dry:
                // output_P(status, PSTR(TOPIC_MODE), PSTR(PAYLOAD_MODE_DRY));
                this->mode = climate::CLIMATE_MODE_DRY;
                break;
            case mode_cool:
                // output_P(status, PSTR(TOPIC_MODE), PSTR(PAYLOAD_MODE_COOL));
                this->mode = climate::CLIMATE_MODE_COOL;
                break;
            case mode_fan:
                // output_P(status, PSTR(TOPIC_MODE), PSTR(PAYLOAD_MODE_FAN));
                this->mode = climate::CLIMATE_MODE_FAN_ONLY;
                break;
            case mode_heat:
                // output_P(status, PSTR(TOPIC_MODE), PSTR(PAYLOAD_MODE_HEAT));
                this->mode = climate::CLIMATE_MODE_HEAT;
                break;
            default:
                ESP_LOGD("mhi_ac_ctrl", "unknown status mode value %i", value);
            }
            this->publish_state();
            break;
        case status_fan:
            switch (value) {
            case 0:
                this->fan_mode = climate::CLIMATE_FAN_QUIET;
                break;
            case 1:
                this->fan_mode = climate::CLIMATE_FAN_LOW;
                break;
            case 2:
                this->fan_mode = climate::CLIMATE_FAN_MEDIUM;
                break;
            case 6:
                this->fan_mode = climate::CLIMATE_FAN_HIGH;
                break;
            case 7:
                this->fan_mode = climate::CLIMATE_FAN_AUTO;
                break;
            }
            this->publish_state();
            break;
            
        case status_vanes: // 20231015 Replaces the orignal switch/case
            if (vanesLR_sensor_value == vanesLR_swing_value) {
                if ( value == vanes_swing ) { 
                    this->swing_mode = climate::CLIMATE_SWING_BOTH;
                } else {
                    this->swing_mode = climate::CLIMATE_SWING_HORIZONTAL;
                    vanes_pos_old_.publish_state(value);
                }
            } else {
                if ( value == vanes_swing ) {
                    this->swing_mode = climate::CLIMATE_SWING_VERTICAL;
                } else {
                    this->swing_mode = climate::CLIMATE_SWING_OFF;
                    vanes_pos_old_.publish_state(value);
                }
            }
            vanes_pos_.publish_state(value);
            this->publish_state();
            break;
            
        case status_vanesLR: // 20231015 Replaces the orignal switch/case
            if (vanesUD_sensor_value == vanesUD_swing_value) {
                if ( value == vanesLR_swing ) { 
                    this->swing_mode = climate::CLIMATE_SWING_BOTH;
                } else {
                    this->swing_mode = climate::CLIMATE_SWING_VERTICAL;
                    vanesLR_pos_old_.publish_state(value);
                }
            } else {
                if ( value == vanesLR_swing ) {
                    this->swing_mode = climate::CLIMATE_SWING_HORIZONTAL;
                } else {
                    this->swing_mode = climate::CLIMATE_SWING_OFF;
                    vanesLR_pos_old_.publish_state(value);
                }
            }
            vanesLR_pos_.publish_state(value);
            this->publish_state();
            break;

        case status_3Dauto:
            switch (value) {
            case 0b00000000:
                Dauto_.publish_state(false);
                break;
            case 0b00000100:
                Dauto_.publish_state(true);
                break;
            }
            this->publish_state();
            break;
        case status_troom:
            this->current_temperature = (value - 61) / 4.0;
            this->publish_state();
            break;
        case status_iu_troom:
            iu_troom_.publish_state((value - 61) / 4.0);
            break;
        case status_tsetpoint:
            this->target_temperature = (value & 0x7f)/ 2.0;
            this->publish_state();
            break;
        case status_errorcode:
        case erropdata_errorcode:
            error_code_.publish_state(value);
            break;
        case opdata_return_air:
        case erropdata_return_air:
            return_air_temperature_.publish_state((value - 61) / 4.0); // 20231103 Jobr suggested
            break;
        case opdata_delta_thermostat:
            delta_thermostat_.publish_state((value - 61) / 4.0);
            //delta_thermostat_.publish_state(value); // debug
            break;
        // Indoor Heat exchanger temperature 1 (U-bend) (capillary/small diameter tube) 20231015
        case opdata_thi_r1:
        case erropdata_thi_r1:
            indoor_unit_thi_r1_.publish_state(value * 0.327f - 11.4f);
            break;
        // 20231015 this is also used for IU with an humidity sensor
        case opdata_thi_r2:
        case erropdata_thi_r2:
            indoor_unit_thi_r2_.publish_state(value * 0.327f - 11.4f);
            break;
        // Indoor Heat exchanger temperature 3 (suction header / big diameter tube) 20231015
        case opdata_thi_r3:
        case erropdata_thi_r3:
            indoor_unit_thi_r3_.publish_state(value * 0.327f - 11.4f);
            break;

        case opdata_iu_fanspeed:
        case erropdata_iu_fanspeed:
            indoor_unit_fan_speed_.publish_state(value);
            break;
        case opdata_total_iu_run:
        case erropdata_total_iu_run:
            indoor_unit_total_run_time_.publish_state(value * 100);
            break;
        case erropdata_outdoor: // tho_r3
        case opdata_outdoor:
            outdoor_temperature_.publish_state((value - 94) * 0.25f);  // 20240113
            //outdoor_temperature_.publish_state(value * 0.327f - 11.4f); // 20231101
            break;
        case opdata_tho_r1:
        case erropdata_tho_r1:
            outdoor_unit_tho_r1_.publish_state((value - 94) * 0.25f);
            //outdoor_unit_tho_r1_.publish_state(0.327f * value - 11.4f);
            break;
        case opdata_comp:
        case erropdata_comp:
            compressor_frequency_.publish_state(highByte(value) * 25.6f + 0.1f * lowByte(value));
            break;
        case erropdata_td:
        case opdata_td:
            if (value < 0x2)
                outdoor_unit_discharge_pipe_.publish_state(30);
            else
                outdoor_unit_discharge_pipe_.publish_state(value / 2 + 32);
            break;
        case opdata_ct:
        case erropdata_ct:
            current_power_.publish_state(value * 14 / 51.0f);
            break;
        case opdata_tdsh:
            outdoor_unit_tdsh_.publish_state(value * 0.327f - 11.4f);
            break;
        // removed 20231006
        //case opdata_tdsh:
        //    outdoor_unit_discharge_pipe_super_heat_.publish_state(value);
        //    break;
        case opdata_protection_no:
            if (value < protection_states.size())
                protection_state_.publish_state(protection_states[value]);
            protection_state_number_.publish_state(value);
            break;
        case opdata_ou_fanspeed:
        case erropdata_ou_fanspeed:
            outdoor_unit_fan_speed_.publish_state(value);
            break;
        case opdata_defrost:
            defrost_.publish_state(value != 0);
            break;
        case opdata_total_comp_run:
        case erropdata_total_comp_run:
            compressor_total_run_time_.publish_state(value * 100);
            break;
        case opdata_ou_eev1:
            outdoor_unit_expansion_valve_.publish_state(value);
            break;
        case erropdata_ou_eev1:
            // itoa(value, strtmp, 10);
            // output_P(status, PSTR(TOPIC_OU_EEV1), strtmp);
            break;
        case opdata_tsetpoint:
        case erropdata_tsetpoint:                   // temporary active
            // I would expected here some code - 20231019
            tsetpoint_offset_corrected_.publish_state((value & 0x7f)/ 2.0);
            //this->tsetpoint_tmp = (value & 0x7f)/ 2.0;
            //this->publish_state();
            break;
        case status_IU_troom_correction: // 20231130 new value
            IU_troom_correction_.publish_state(value / 4.0);
            //vanes_pos_.publish_state(value);
            //this->publish_state();
            break;
        case opdata_kwh:
            // https://github.com/absalom-muc/MHI-AC-Ctrl/pull/135
            // This item is counting the kWh from the point where the AC is powered On
            //energy_used_.publish_state(value * 0.25); // 2023107 changed to the line below
            energy_used_.publish_state(highByte(value) * 64.0f + 0.25f * lowByte(value));
            break;
        case opdata_unknown:
            // skip these values as they are not used currently
            break;
        }
    }

    std::vector<Sensor *> get_sensors() {
        return {
            &error_code_,
            &outdoor_temperature_,
            &return_air_temperature_,
            &outdoor_unit_fan_speed_,
            &indoor_unit_fan_speed_,
            &current_power_,
            &compressor_frequency_,
            &indoor_unit_total_run_time_,
            &compressor_total_run_time_,
            &vanes_pos_,
            &energy_used_,
            &indoor_unit_thi_r1_,
            &indoor_unit_thi_r2_,
            &indoor_unit_thi_r3_,
            &outdoor_unit_tho_r1_,
            &outdoor_unit_expansion_valve_,
            &outdoor_unit_discharge_pipe_,
            &outdoor_unit_tdsh_,
            &protection_state_number_,
            &vanesLR_pos_,
            &Dauto_,
            &tsetpoint_offset_corrected_,
            &delta_thermostat_,
            &iu_troom_,
            &IU_troom_correction_,
        };
    }

    std::vector<TextSensor *> get_text_sensors() {
        return {
            &protection_state_,
            &vanesUD_text_,
            &vanesLR_text_,
        };
    }

    std::vector<BinarySensor *> get_binary_sensors() {
        return { &defrost_ };
    }

    void set_room_temperature(float value) {
        if ((value > -10) && (value < 48)) {
            room_temp_api_timeout_ms = millis();  // reset timeout
            value= trunc(value*4)/4; // 20231029 truncing to 0.25 by using trunc a small overshoot is created
            value= float(value*4)/4; // 20240205 correctly
            mhi_ac_ctrl_core.set_troom(value*4+61);
            ESP_LOGD("mhi_ac_ctrl", "use Troom sensor to for room_temp_api: %.2f°C", value );
            //ESP_LOGD("mhi_ac_ctrl", "set room_temp_api: %f %i %s", value, (byte)(value*4+61), "C);
        }
    }
    // allows to manual change the temp_correction and overwrite the default (static) correction
    void set_troom_correction(float value) {
        value = (rint(value*4)/4)*4 ; // truncing to 0.25 degrees and prepares it for CorePlus
        correct_troom = value; // overwrite default with manual value
        mhi_ac_ctrl_core.set_temp_correction(value);
        ESP_LOGD("mhi_ac_ctrl", "apply correction to Troom core value: %.2f°C", (value / 4));
    }
    
    // allows to manual change the IU temp_correction and overwrite the default (static)  IU correction
    void set_temp_IU_correction(float value) {
        value = (rint(value*4)/4)*4 ; // truncing to 0.25 degrees and prepares it for CorePlus
        IU_correct_troom = value; // overwrite any default with value
        mhi_ac_ctrl_core.set_temp_IU_correction(value);
        ESP_LOGD("mhi_ac_ctrl", "apply correction to IU Troom core value: %.2f°C", (value / 4));
    }

    void set_vanes(int value) {
        mhi_ac_ctrl_core.set_vanes(value);
        ESP_LOGD("mhi_ac_ctrl", "set vanes: %i", value);
    }

    void set_vanesLR(int value) {
        mhi_ac_ctrl_core.set_vanesLR(value);
        ESP_LOGD("mhi_ac_ctrl", "set vanes Left Right: %i", value);
    }

    void set_3Dauto(bool value) {
        // ESP_LOGD("mhi_ac_ctrl", "set 3D auto: %s", value);
        if (value){
            ESP_LOGD("mhi_ac_ctrl", "set 3D auto: on");
            mhi_ac_ctrl_core.set_3Dauto(AC3Dauto::Dauto_on); // Set swing to 3Dauto
        }
        else {
            ESP_LOGD("mhi_ac_ctrl", "set 3D auto: off");
            mhi_ac_ctrl_core.set_3Dauto(AC3Dauto::Dauto_off); // Set swing to 3Dauto
        }
        ESP_LOGD("mhi_ac_ctrl", "set vanes Left Right: %i", value);
    }
    
    void set_tsetpoint_offset(float value) { // 20231206 
        value = rint((value * 2) + 5); // gives a range from -2.5 to 2.5 -> 0..10
        if (value <= 10 || value >= 0) { 
          mhi_ac_ctrl_core.set_tsetpoint((byte)(value)); 
          ESP_LOGD("mhi_ac_ctrl", "set tsetpoint_offset: %i", value);
        }
    }
    
//    void set_manual_defrost(bool value) { // 20231207 
//        mhi_ac_ctrl_core.set_dummy_defrost((bool)(value));
//        ESP_LOGD("mhi_ac_ctrl", "defrost active: %s", value);
//    }
    
protected:
    // Transmit the state of this climate controller.
    void control(const climate::ClimateCall& call) override
    {
        if (call.get_mode().has_value()) {
            this->mode = *call.get_mode();

            power_ = power_on;
            switch (this->mode) {
            case climate::CLIMATE_MODE_OFF:
                power_ = power_off;
                break;
            case climate::CLIMATE_MODE_COOL:
                mode_ = mode_cool;
                break;
            case climate::CLIMATE_MODE_HEAT:
                mode_ = mode_heat;
                break;
            case climate::CLIMATE_MODE_DRY:
                mode_ = mode_dry;
                break;
            case climate::CLIMATE_MODE_FAN_ONLY:
                mode_ = mode_fan;
                break;
            case climate::CLIMATE_MODE_HEAT_COOL:
            default:
                mode_ = mode_auto;
                break;
            }

            mhi_ac_ctrl_core.set_power(power_);
            mhi_ac_ctrl_core.set_mode(mode_);
            
            // 20231014 correctly the displayed temperature range in panels and webinterface, enables to be set below 18 degrees when the mode is HEAT
            // needs a adapted version of MHI-AC-Ctrl-core.cpp to support this natively 
            this->minimum_temperature_ = ( mode_ == mode_heat && id(low_temp_heating) ) ? 10.0f : 18.0f;    // shorthand c++, set minimum_temperature bases on the set mode of the unit
            // if ( mode_ == mode_heat && low_temperature_heating ) { this->minimum_temperature_ = 10.0f;   // used in the panels/gauges as minimal settable temperature
            // } else { this->minimum_temperature_ = 18.0f; }                                               // when mode is HEAT a lower minimum temperature can be used (change it back to 18degrees to disable this)
            tsetpoint_ = (float)clamp(this->target_temperature, minimum_temperature_, maximum_temperature_);// ready tsetpoint_ for the next transmit to the core
        }

        if (call.get_target_temperature().has_value()) {
            this->target_temperature = *call.get_target_temperature();
            tsetpoint_ = clamp(this->target_temperature, minimum_temperature_, maximum_temperature_);
            this->target_temperature=tsetpoint_;    // 20231019 enables the Core to set back to 18 degrees if not in mode HEAT and more. Value has been set above
            mhi_ac_ctrl_core.set_tsetpoint((byte)(2 * tsetpoint_));
            mhi_ac_ctrl_core.set_lowtempheating((bool)(id(low_temp_heating)));  // enable/disable the possibility to heat between 10 and 18 degress celcius
        }

        if (call.get_fan_mode().has_value()) {
            this->fan_mode = *call.get_fan_mode();

            switch (*this->fan_mode) {
            case climate::CLIMATE_FAN_QUIET:
                fan_ = 0;
                break;
            case climate::CLIMATE_FAN_LOW:
                fan_ = 1;
                break;
            case climate::CLIMATE_FAN_MEDIUM:
                fan_ = 2;
                break;
            case climate::CLIMATE_FAN_HIGH:
                fan_ = 6;
                break;
            case climate::CLIMATE_FAN_AUTO:
            default:
                fan_ = 7;
                break;
            }

            mhi_ac_ctrl_core.set_fan(fan_);
        }

        if (call.get_swing_mode().has_value()) {
            this->swing_mode = *call.get_swing_mode();
            int vanesLR_pos_old_value = vanesLR_pos_old_.has_state() ? vanesLR_pos_old_.state : 4;
            int vanes_pos_old_value = vanes_pos_old_.has_state() ? vanes_pos_old_.state : 4;
            vanesLR_ = static_cast<ACVanesLR>(vanesLR_pos_old_value);
            vanes_ = static_cast<ACVanes>(vanes_pos_old_value);

            switch (this->swing_mode) {
            case climate::CLIMATE_SWING_OFF:
                break;
            case climate::CLIMATE_SWING_VERTICAL:
                vanes_ = vanes_swing;
                break;
            case climate::CLIMATE_SWING_HORIZONTAL:
                vanesLR_ = vanesLR_swing;
                break;
            default:
            case climate::CLIMATE_SWING_BOTH:
                vanesLR_ = vanesLR_swing;
                vanes_ = vanes_swing;
                break;
            }
            mhi_ac_ctrl_core.set_vanesLR(vanesLR_); // Set vanesLR to swing
            mhi_ac_ctrl_core.set_vanes(vanes_); // Set vanes to swing
        }
        this->publish_state();
    }

    /// Return the traits of this controller.
    climate::ClimateTraits traits() override
    {
        auto traits = climate::ClimateTraits();
        traits.set_supports_current_temperature(true);
        traits.set_supported_modes({ CLIMATE_MODE_OFF, CLIMATE_MODE_HEAT_COOL, CLIMATE_MODE_COOL, CLIMATE_MODE_HEAT, CLIMATE_MODE_DRY, CLIMATE_MODE_FAN_ONLY });
        traits.set_supports_two_point_target_temperature(false);
        traits.set_visual_min_temperature(this->minimum_temperature_);
        traits.set_visual_max_temperature(this->maximum_temperature_);
        traits.set_visual_temperature_step(this->temperature_step_);
        traits.set_supported_fan_modes({ CLIMATE_FAN_AUTO, CLIMATE_FAN_QUIET, CLIMATE_FAN_LOW, CLIMATE_FAN_MEDIUM, CLIMATE_FAN_HIGH });
        traits.set_supported_swing_modes({ CLIMATE_SWING_OFF, CLIMATE_SWING_BOTH, CLIMATE_SWING_VERTICAL, CLIMATE_SWING_HORIZONTAL });
        return traits;
    }

    float minimum_temperature_ { 18.0f };
    float maximum_temperature_ { 30.0f };
    float temperature_step_ { 0.5f };

    ACPower power_;
    ACMode mode_;
    float tsetpoint_; // arrives in core multiplied by 2 and there it is a uint
    uint fan_;
    ACVanes vanes_;
    ACVanesLR vanesLR_;

    MHI_AC_Ctrl_Core mhi_ac_ctrl_core;

    Sensor error_code_;
    Sensor outdoor_temperature_;
    Sensor return_air_temperature_;
    Sensor outdoor_unit_fan_speed_;
    Sensor indoor_unit_fan_speed_;
    Sensor compressor_frequency_;
    Sensor indoor_unit_total_run_time_;
    Sensor compressor_total_run_time_;
    Sensor current_power_;
    BinarySensor defrost_;
    Sensor vanes_pos_;
    Sensor vanes_pos_old_;
    Sensor energy_used_;
    Sensor indoor_unit_thi_r1_;
    Sensor indoor_unit_thi_r2_;
    Sensor indoor_unit_thi_r3_;
    Sensor outdoor_unit_tho_r1_;
    Sensor outdoor_unit_expansion_valve_;
    Sensor outdoor_unit_discharge_pipe_;
    Sensor outdoor_unit_tdsh_;
    Sensor protection_state_number_;
    TextSensor protection_state_;
    Sensor vanesLR_pos_;
    Sensor vanesLR_pos_old_;
    Sensor Dauto_;
    TextSensor vanesUD_text_;
    TextSensor vanesLR_text_;
    Sensor tsetpoint_offset_corrected_; //20231001
    Sensor delta_thermostat_; //20231101
    Sensor iu_troom_; //20231121
    Sensor IU_troom_correction_; //20231130
};
