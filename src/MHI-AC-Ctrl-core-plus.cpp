// MHI-AC-Ctrl-core-plus -  new name, enbling to choose which version you want to use
// implements the core functions (read & write SPI), adapted to have new functionality that
// is close to the core and the MHI hardware.
// allowing setting temperature below 18 degrees celcius when mode is set to HEAT

static const char* versionCoreCPP = "20240223-2.8Plus";

#include "MHI-AC-Ctrl-core-plus.h"

uint16_t calc_checksum(byte* frame) {
  uint16_t checksum = 0;
  for (int i = 0; i < CBH; i++)
    checksum += frame[i];
  return checksum;
}

uint16_t calc_checksumFrame33(byte* frame) {
  uint16_t checksum = 0;
  for (int i = 0; i < CBL2; i++)
    checksum += frame[i];
  return checksum;
}

void MHI_AC_Ctrl_Core::reset_old_values() {  // used e.g. when MQTT connection to broker is lost, to re-output data
  // old status
  status_power_old = 0xff;
  status_mode_old = 0xff;
  status_fan_old = 0xff;
  status_vanes_old = 0xff;
  status_troom_old = 0xfe; // different than the default value of 0xff
  status_iu_troom_old = 0x00; // 20231122 new variable
  status_tsetpoint_old = 0x00;
  status_errorcode_old = 0xff;
  status_vanesLR_old = 0xff;
  status_3Dauto_old = 0xff;
  tsetpoint_tmp_old = 0xff;

  // old operating data
  op_kwh_old = 0xffff;
  op_mode_old = 0xff;
  op_settemp_old = 0xff;
  op_return_air_old = 0xff;
  op_iu_fanspeed_old = 0xff;
  op_thi_r1_old = 0x00;
  op_thi_r2_old = 0x00;
  op_thi_r3_old = 0x00;
  op_total_iu_run_old = 0;
  op_outdoor_old = 0xff;
  op_tho_r1_old = 0x00;
  op_total_comp_run_old = 0;
  op_ct_old = 0xff;
  op_tdsh_old = 0xff;
  op_protection_no_old = 0xff;
  op_ou_fanspeed_old = 0xff;
  op_defrost_old = 0x00;
  op_comp_old = 0xffff;
  op_td_old  = 0x00;
  op_ou_eev1_old = 0xffff;
  
  old_Troom = 0xfe; // set to a neutral value
}

void MHI_AC_Ctrl_Core::init() {
  //MeasureFrequency(m_cbiStatus);
  pinMode(SCK_PIN, INPUT);
  pinMode(MOSI_PIN, INPUT);
  pinMode(MISO_PIN, OUTPUT);
  MHI_AC_Ctrl_Core::reset_old_values();
 // m_cbiStatus->cbiStatusFunction(version, versionCoreH);
}

static byte debug_value;

//##################### New Core plus code ########################################
void MHI_AC_Ctrl_Core::set_lowtempheating(boolean lowtempheat) {        // 20231028 enables to en/disable the support for heating below 18 degrees
  low_temp_heating = lowtempheat;
}

void MHI_AC_Ctrl_Core::set_temp_correction(byte correct_troom) {        // 20231028 correct the room temperature and should arrive, being multiplied by four (0.25)
  troom_correction = 0;                                                 // first reset any active corrections
  if ( status_mode_old == mode_heat )                                   // only apply if mode is heat. this can be made for all wokingmodes by marking out this line
    troom_correction = correct_troom;
}

void MHI_AC_Ctrl_Core::set_temp_IU_correction(byte correct_IU_troom) {  // 20231028 enables to correct the troom to 'calibrate' to thermostat. value should arrive being multiplied by four
  IU_troom_tmp += IU_troom_correction;                                  // first remove any (old) corrections now a new value arrives
  if ( status_mode_old == mode_heat ) {                                 // only apply when mode is heat
    if ( correct_IU_troom < 0x28 ) {                                    // apply as offset when below 10 (0x28) multiplied by four
      IU_troom_correction = correct_IU_troom;                           // apply the offset
    } else {
      if ( IU_troom_tmp <= correct_IU_troom + 61 )                      // check if the measured temperature is higher than the stated correct temperature
        IU_troom_correction = IU_troom_tmp - (correct_IU_troom + 61);   // calculate the offset. adding 61 to bring to the same level as the here used temperature values
    }
    IU_troom_tmp -= IU_troom_correction;                                // apply the previous/new IU sensor temperature correction till next recalculation of the IU tempearature
  }
}
//################### End new Core plus code ######################################

void MHI_AC_Ctrl_Core::set_power(boolean power) {
  new_Power = 0b10 | power;
}

//##################### Changed Core code #########################################
// 20231023 added control to enable or disable low temperature when heating (low_temp_heating)
void MHI_AC_Ctrl_Core::set_mode(ACMode mode) {
  if ((status_mode_old!=mode_heat || low_temp_heating==false) && tsetpoint_tmp<36) new_Tsetpoint=0b10000000 | 36;   // enforces 18 degrees minimum if needed
  new_Mode = 0b00100000 | mode;
}

// 20231018 revised version. Changes by msatter, enabling direct support for heat below 18 degrees and using half degrees, based on tsetpoint
void MHI_AC_Ctrl_Core::set_tsetpoint(uint tsetpoint) {                                              // tsetpoint arrives as a value already multiplied by two
  if (tsetpoint<=10 && status_troom_old>=111) {tsetpoint=((status_troom_old-61+offset_Troom)/2)+(tsetpoint-5);}  // use offset to calculate new tsetpoint (range -2.5 to 2.5) (111 -> 12.5 in troom)
  if ( tsetpoint >= 20 ) {                                                                          // if invalid (<10) then do nothing
    if ((status_mode_old!=mode_heat || low_temp_heating==false) && tsetpoint<36) tsetpoint = 36;    // enforces 18 degrees minimum when low_temp_heat is not allowed
    tsetpoint_tmp = tsetpoint;
    offset_Troom=(status_mode_old==mode_heat&&tsetpoint<36)?((36-tsetpoint)*2):((tsetpoint&1)?2:0); // used below 18 degrees or only the offset. Troom is default multiplied by four so there the times two is there (compressed shorwriting)
    if ( offset_Troom > 0 && new_Troom != 0xff ) new_Troom=troom_tmp+offset_Troom-troom_correction; // publish a new_Troom to have a possible faster response from the MHI. does not break IU sensor reading
    new_Tsetpoint = 0b10000000 | tsetpoint;                                                         // original line to set new_Tsetpoint
  }
}

// 20230310 offset_Troom is added to troom on half degrees and heat below 18 degrees. 0xff signals a IU temperature read
void MHI_AC_Ctrl_Core::set_troom(byte troom) {                                                      // troom arrives as a value multiplied by four
  if (troom != 0xff) {                                                                              // core-plus has 0xff covered so no need anymore for external triggering of the IU sensor
    troom_tmp = troom;                                                                              // used in other parts of core-plus
    offset_Troom=(status_mode_old==mode_heat&&tsetpoint_tmp<36)?((36-tsetpoint_tmp)*2):((tsetpoint_tmp&1)?2:0); // used below 18 degrees or only the offset. Troom is default multiplied by four so there the times two is there (compressed shorwriting)
    troom += offset_Troom - troom_correction;                                                       // do not use offset when the IU temperature sensor is used and also checks if low_temp_heating is enabled
    lastTroomInternalMillis = millis();                                                             // setting an other +40 seconds waiting time before the IU sensor is being read (again)
    IU_sample_count = 0;                                                                            // 20240201 resets the counter for IU temperature sensor samples, to avoid extra readings leading then to spikes
    EXT_Troom = troom;                                                                              // temporary storing the ext. troom value
    new_Troom = 0xff;                                                                               // forcing reading of the IU sensor to have separte value later
    IU_spreader = 10;                                                                               // 20240130 sets pause before IU is read when the external temp. sensor is active
    if ( IU_Spreader_count == 99 ) IU_Spreader_count = 0;                                           // 20240130 reset the counter when external temperature sensor is used and IU sensor was being used
    if ( IU_Spreader_count < IU_spreader ) new_Troom = troom;                                       // 20240129 allows to regulate the sampling off the IU temperature sensor
    IU_Spreader_count++;                                                                            // 20240129 counts the number of external troom where received
  }
}
//######################## End changed Core code ##################################

void MHI_AC_Ctrl_Core::set_fan(uint fan) {
  new_Fan = 0b00001000 | fan;
}

void MHI_AC_Ctrl_Core::set_3Dauto(AC3Dauto Dauto) {
  new_3Dauto = 0b00001010 | Dauto;
}

void MHI_AC_Ctrl_Core::set_vanes(uint vanes) {
  if (vanes == vanes_swing) {
    new_Vanes0 = 0b11000000; // enable swing
  }
  else {
    new_Vanes0 = 0b10000000; // disable swing
    new_Vanes1 = 0b10000000 | ((vanes - 1) << 4);
  }
}

void MHI_AC_Ctrl_Core::set_vanesLR(uint vanesLR) {
  if (vanesLR == vanesLR_swing) {
    new_VanesLR0 = 0b00001011; // enable swing
  }
  else {
    new_VanesLR0 = 0b00001010; // disable swing
    new_VanesLR1 = 0b00010000 | (vanesLR - 1);
  }
}

void MHI_AC_Ctrl_Core::request_ErrOpData() {
  request_erropData = true;
}

float MHI_AC_Ctrl_Core::get_troom_offset() {
  return Troom_offset;
}

void MHI_AC_Ctrl_Core::set_troom_offset(float offset) {
  Troom_offset = offset;
}

void MHI_AC_Ctrl_Core::set_frame_size(byte framesize) {
  if (framesize == 20 || framesize == 33)
    frameSize = framesize;
}

int MHI_AC_Ctrl_Core::loop(uint max_time_ms) {              // increased from 100ms to 150ms in caller to give more time when using 33 frames.
  const byte opdataCnt = sizeof(opdata) / sizeof(byte) / 2;
  static byte opdataNo = 0;
  long startMillis = millis();              // start time of this loop run
  byte MOSI_byte;                           // received MOSI byte
  bool new_datapacket_received = false;     // indicated that a new frame was received
  static byte erropdataCnt = 0;             // number of expected error operating data
  static bool doubleframe = false;
  static int frame = 1;
  static byte MOSI_frame[33];
  //                            sb0   sb1   sb2   db0   db1   db2   db3   db4   db5   db6   db7   db8   db9  db10  db11  db12  db13  db14  chkH  chkL  db15  db16  db17  db18  db19  db20  db21  db22  db23  db24  db25  db26  chk2L
  static byte MISO_frame[] = { 0xA9, 0x00, 0x07, 0x00, 0x00, 0x00, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff, 0x0f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff, 0x22 };

  static uint call_counter = 0;                     // counts how often this loop was called

  if (frameSize == 33)
    MISO_frame[0] = 0xAA;

  call_counter++;
  int SCKMillis = millis();                         // time of last SCK low level
  while (millis() - SCKMillis < 5) {                // wait for 5ms stable high signal to detect a frame start
    if (!digitalRead(SCK_PIN))
      SCKMillis = millis();
    if (millis() - startMillis > max_time_ms)
      return err_msg_timeout_SCK_low;               // SCK stuck@ low error detection
  }
  // build the next MISO frame

  doubleframe = !doubleframe;                       // toggle every frame
  MISO_frame[DB14] = doubleframe << 2;              // MISO_frame[DB14] bit2 toggles with every frame
  
  // Requesting all different opdata's is an opdata cycle. A cycle will take 20s.
  // With the current 20 different opdata's, every opdata request will take 1sec (interval).
  // If there are only 5 different opdata's defined, these 5 will be spread about the 20s cycle. The interval will increase.
  // requesting a new opdata will always start at a doubleframe start
  if ((frame > (NoFramesPerOpDataCycle / opdataCnt)) && doubleframe ) {     // interval for requesting new opdata depending on de number of opdata requests
    frame = 1;                                                              // start requesting new OpData
  }

  if (frame++ <= 2) {                               // use opdata request only for 2 subsequent frames
    if (doubleframe) {                              // start when MISO_frame[DB14] bit2 is set
      if (erropdataCnt == 0) {
        MISO_frame[DB6] = pgm_read_word(opdata + opdataNo);
        MISO_frame[DB9] = pgm_read_word(opdata + opdataNo) >> 8;
        opdataNo = (opdataNo + 1) % opdataCnt;
      }
    }
  }
  else  // reset OpData request
  {
    MISO_frame[DB6] = 0x80;
    MISO_frame[DB9] = 0xff;    
  }
  
  if (doubleframe) {                                // and the other MISO data changes are updated when MISO_frame[DB14] bit2 is set
    MISO_frame[DB0] = 0x00;
    MISO_frame[DB1] = 0x00;
    MISO_frame[DB2] = 0x00;

    if (erropdataCnt > 0) {                         // error operating data available
      MISO_frame[DB6] = 0x80;
      MISO_frame[DB9] = 0xff;
      erropdataCnt--;
    }

    // set Power, Mode, Tsetpoint, Fan, Vanes
    MISO_frame[DB0] = new_Power;
    new_Power = 0;

    MISO_frame[DB0] |= new_Mode;
    new_Mode = 0;

    MISO_frame[DB2] = new_Tsetpoint;
    new_Tsetpoint = 0;

    MISO_frame[DB1] = new_Fan;
    new_Fan = 0;

    MISO_frame[DB0] |= new_Vanes0;
    MISO_frame[DB1] |= new_Vanes1;
    new_Vanes0 = 0;
    new_Vanes1 = 0;

    if (request_erropData) {
      MISO_frame[DB6] = 0x80;
      MISO_frame[DB9] = 0x45;
      request_erropData = false;
    }
  }
  // Room temperature either supplied through MQTT, DS18x20 or IU, available in Core-Plus
  // 20240302 when 0xff is active don't restart it. Plus will overrule this by promoting inner temperature to new_Troom every loop
  if ( new_Troom != 0xff || (new_Troom == 0xff && old_Troom != 0xff) ) { 
    MISO_frame[DB3] = new_Troom;
    old_Troom = new_Troom;
  }

  uint16_t checksum = calc_checksum(MISO_frame);
  MISO_frame[CBH] = highByte(checksum);
  MISO_frame[CBL] = lowByte(checksum);

  if (frameSize == 33) { // Only for framesize 33 (WF-RAC)
    MISO_frame[DB16] = 0;
    MISO_frame[DB16] |= new_VanesLR1;
    MISO_frame[DB17] = 0;
    MISO_frame[DB17] |= new_VanesLR0;  
    MISO_frame[DB17] |= new_3Dauto;
    new_3Dauto = 0;
    new_VanesLR0 = 0;
    new_VanesLR1 = 0;

    checksum = calc_checksumFrame33(MISO_frame);
    MISO_frame[CBL2] = lowByte(checksum);
  }
  //Serial.println();
  //Serial.print(F("MISO:"));
  // read/write MOSI/MISO frame
  for (uint8_t byte_cnt = 0; byte_cnt < frameSize; byte_cnt++) { // read and write a data packet of 20 bytes
    //Serial.printf("x%02x ", MISO_frame[byte_cnt]);
    MOSI_byte = 0;
    byte bit_mask = 1;
    for (uint8_t bit_cnt = 0; bit_cnt < 8; bit_cnt++) { // read and write 1 byte
      SCKMillis = millis();
      while (digitalRead(SCK_PIN)) { // wait for falling edge
        if (millis() - startMillis > max_time_ms)
          return err_msg_timeout_SCK_high;       // SCK stuck@ high error detection
      } 
      if ((MISO_frame[byte_cnt] & bit_mask) > 0)
        digitalWrite(MISO_PIN, 1);
      else
        digitalWrite(MISO_PIN, 0);
      while (!digitalRead(SCK_PIN)) {} // wait for rising edge
      if (digitalRead(MOSI_PIN))
        MOSI_byte += bit_mask;
      bit_mask = bit_mask << 1;
    }
    if (MOSI_frame[byte_cnt] != MOSI_byte) {
      new_datapacket_received = true;
      MOSI_frame[byte_cnt] = MOSI_byte;
    }
  }

  checksum = calc_checksum(MOSI_frame);
  if (((MOSI_frame[SB0] & 0xfe) != 0x6c) | (MOSI_frame[SB1] != 0x80) | (MOSI_frame[SB2] != 0x04))
    return err_msg_invalid_signature;
  if ((MOSI_frame[CBH] << 8 | MOSI_frame[CBL]) != checksum)
    return err_msg_invalid_checksum;

  if (frameSize == 33) { // Only for framesize 33 (WF-RAC)
    checksum = calc_checksumFrame33(MOSI_frame);
    if ( MOSI_frame[CBL2] != lowByte(checksum ) ) 
      return err_msg_invalid_checksum;
  }

  if (new_datapacket_received) {

    if (frameSize == 33) { // Only for framesize 33 (WF-RAC)
      byte vanesLRtmp = (MOSI_frame[DB16] & 0x07) + ((MOSI_frame[DB17] & 0x01) << 4);
      if (vanesLRtmp != status_vanesLR_old) { // Vanes Left Right
        if ((vanesLRtmp & 0x10) != 0) // Vanes LR status swing
          m_cbiStatus->cbiStatusFunction(status_vanesLR, vanesLR_swing);
        else {
          m_cbiStatus->cbiStatusFunction(status_vanesLR, (vanesLRtmp & 0x07) + 1 );
        }
        status_vanesLR_old = vanesLRtmp;
      }

      if ((MOSI_frame[DB17] & 0x04) != status_3Dauto_old) { // 3D auto
        status_3Dauto_old = MOSI_frame[DB17] & 0x04;
        m_cbiStatus->cbiStatusFunction(status_3Dauto, status_3Dauto_old);
      }
    }
//#################### changed code ###############################################
    // evaluate status
    if ((MOSI_frame[DB0] & 0x1c) != status_mode_old) {                          // Mode HEAT specific code
      if ( status_mode_old == mode_heat ) {                                     // check if previous mode was heat
        IU_troom_tmp += IU_troom_correction;                                    // before reset undo an earlier applied IU-correction
        IU_troom_correction = 0;                                                // reset any active correction when exiting heating mode
      }
      status_mode_old = MOSI_frame[DB0] & 0x1c;
      m_cbiStatus->cbiStatusFunction(status_mode, status_mode_old);
    }
//#################################################################################

    if ((MOSI_frame[DB0] & 0x01) != status_power_old) { // Power
      status_power_old = MOSI_frame[DB0] & 0x01;
      m_cbiStatus->cbiStatusFunction(status_power, status_power_old);
    }

    uint fantmp = MOSI_frame[DB1] & 0x07;
    if (fantmp != status_fan_old) {
      status_fan_old = fantmp;
      m_cbiStatus->cbiStatusFunction(status_fan, status_fan_old);
    }

    // Only updated when Vanes command via wired RC
    uint vanestmp = (MOSI_frame[DB0] & 0xc0) + ((MOSI_frame[DB1] & 0xB0) >> 4);
    if (vanestmp != status_vanes_old) {
      // if ((vanestmp & 0x88) == 0) // last vanes update was via IR-RC, so status is not known
      //   m_cbiStatus->cbiStatusFunction(status_vanes, vanes_unknown);
      // else 
      if ((vanestmp & 0x40) != 0) {// Vanes status swing
        m_cbiStatus->cbiStatusFunction(status_vanes, vanes_swing);
      } else {
        m_cbiStatus->cbiStatusFunction(status_vanes, (vanestmp & 0x03) + 1);
      }
      status_vanes_old = vanestmp;
    }
// #############################################################################
// this code allows to sample the IU sensor and then promote this to a Troom reading
// allowing so to manipulate the value to allow an offset and/or heat below 18 degrees Celcius

    loop_interval = (unsigned long)(millis() - lastTroomInternalMillis);        // saving passed time since start of timer to be used later on, saving so processing time

    // Checks, if the IU temperature sensor is activ and the sample avarage is promoted to new_Troom
    if ( new_Troom == 0xff ) {                                                  // check first if the IU sensor is being activated (saves processing time)
      if ( loop_interval > 1 && loop_interval < 5000 ) {                        // sampling has to completed within 5 seconds
        IU_sample_count++;
        IU_sample += MOSI_frame[DB3];                                           // add the just sampled IU sensor to earlier sampled value(s)
        if ( IU_sample_count == get_samples ) {                                 // after needed number of samples, take the avarage of them
          IU_Troom = (rint((IU_sample/get_samples)*4)/4);                       // store IU raw temperature to be used as pseudo Troom and used to set IU new_Troom (0.25)
          IU_troom_tmp = IU_Troom - IU_troom_correction;                        // when mode -> heat then also apply the IU correction
          // used below 18 degrees or only the offset. Troom is default multiplied by four so there the times two is there (compressed shortwriting)
          offset_Troom=(status_mode_old==mode_heat&&tsetpoint_tmp<36)?((36-tsetpoint_tmp)*2):((tsetpoint_tmp&1)?2:0);
          new_Troom = IU_troom_tmp + offset_Troom;                              // apply the psuedo troom, active during the current loop, if not interupted by an external troom
          // if in the period of sampling an external sensor value is received then use that value instead of the IU sensor value
          if ( EXT_Troom != 0 ) {                                               // chedk if in the meantime external temperature has been received, overwriting the sampled iu_Troom
            new_Troom = EXT_Troom;                                              // overwrite the value set above from the IU sensor if an external sensor is active
            EXT_Troom = 0;                                                      // clear the variable enabling to indicate that a external sensor value is received
          }
          IU_sample = 0;                                                        // resets the counted external troom samples
        }
      }
    } // endif new_Troom = 0xff. As soon the new_troom is set it us used and reading hte IU sensor (0xff) is deactived automaticaly
    
    // the exact match of IU_sample_count to the value should filter out spikes/measurement errors, accepts only values after 5 seconds timeout
    if  ( (loop_interval > 5000 && IU_sample_count == get_samples) ||                   // before publishing the IU sensor and Troom the sampling has to be completed or it will not be transmitted
            IU_Spreader_count < IU_spreader ) {                                         // when external sensor is active then this will instantly be tranmitted 
      if ( MOSI_frame[DB3]!= status_troom_old && IU_Spreader_count < IU_spreader) {     // 20240129 added IU_Spreader_count to not sample each time also the IU sensor 
        status_troom_old = MOSI_frame[DB3];
        m_cbiStatus->cbiStatusFunction(status_troom, status_troom_old - offset_Troom);  // publishes external/internal sensor temperature
      }
      // publish the IU_Troom and current applied correction
      if ( IU_Troom != status_iu_troom_old && IU_sample_count == get_samples ) {        // 20240129 IU temperature is transmited without the IU correction being applied
        IU_Spreader_count = 0;                                                          // 20240129 
        status_iu_troom_old = IU_Troom + troom_correction;                              // adding the troom_correction to have a match again with room temperature 20240114
        m_cbiStatus->cbiStatusFunction(status_iu_troom, status_iu_troom_old);           // IU temperature sensor value is being send
        m_cbiStatus->cbiStatusFunction(status_IU_troom_correction, IU_troom_correction);// offset, still being multiplied by four to be able to state 0.25 resolution
      }
    IU_sample_count = 0;    // indicates that sampling has completed succesfully 
    }
    // this will trigger that only the IU temperature will be used to determine the temperature in the room
    if ( loop_interval > minTimeInternalTroom ) {                       // when minTimeInternalTroom loop waiting time has passed without an external arriving in the meantime
      new_Troom = 0xff;                                                 // triggers reading the IU temperature sensor
      IU_sample_count = 0;                                              // resets the counter for IU temperature sensor samples
      IU_Spreader_count = 99;                                           // use only the IU sensor from now till external temperature sensor becomes active again
      IU_spreader = 99;                                                 // use only the IU sensor till an external temperature sensor value is being received
      lastTroomInternalMillis = millis();                               // sets a new starting time
    }

// ######################## Changed Core plus code ################################
    if (MOSI_frame[DB2] != status_tsetpoint_old) {                                  // Temperature setpoint
      status_tsetpoint_old = MOSI_frame[DB2];
      m_cbiStatus->cbiStatusFunction(status_tsetpoint, status_tsetpoint_old);
    }
// ######################## End changed Core plus code ############################

    if (MOSI_frame[DB4] != status_errorcode_old) { // error code
      status_errorcode_old = MOSI_frame[DB4];
      m_cbiStatus->cbiStatusFunction(status_errorcode, status_errorcode_old);
    }

    // Evaluate Operating Data and Error Operating Data
    bool MOSI_type_opdata = (MOSI_frame[DB10] & 0x30) == 0x10;

    switch (MOSI_frame[DB9]) {
      case 0x94:                              // 0 energy-kwh n * 0.25 kWh used since power on
        if ((MOSI_frame[DB6] & 0x80) != 0) { 
            if (MOSI_type_opdata) {
            if (((MOSI_frame[DB12]<<8)+(MOSI_frame[DB11])) != op_kwh_old) {
              op_kwh_old = (MOSI_frame[DB12]<<8)+(MOSI_frame[DB11]);
              m_cbiStatus->cbiStatusFunction(opdata_kwh, op_kwh_old);
            }
          }
          //else
          //  m_cbiStatus->cbiStatusFunction(erropdata_unknown, op_unknown_old);  // noch nie gesehen, dass es auftaucht
        }
        break;
      case 0x02:
        if ((MOSI_frame[DB6] & 0x80) != 0) {  // 1 MODE
          if (MOSI_type_opdata) {
            if ((MOSI_frame[DB10] != op_mode_old)) {
              op_mode_old = MOSI_frame[DB10];
              m_cbiStatus->cbiStatusFunction(opdata_mode, (op_mode_old & 0x0f) << 2);
            }
          }
          else
            m_cbiStatus->cbiStatusFunction(erropdata_mode, (MOSI_frame[DB10] & 0x0f) << 2);
        }
        break;
      case 0x05: // corrected by the troom offset to also display and only display half degrees and below 18 degrees correctly 20231103
        if ((MOSI_frame[DB6] & 0x80) != 0) {  // 2 SET-TEMP
          if (MOSI_frame[DB10] == 0x13) {
            if (MOSI_frame[DB11] != op_settemp_old) {
              op_settemp_old = MOSI_frame[DB11];
              m_cbiStatus->cbiStatusFunction(opdata_tsetpoint, (op_settemp_old - offset_Troom) ); // reactivated in mhi-ac-ctrl.h - 20231019
            }
          }
          else if (MOSI_frame[DB10] == 0x33)
            m_cbiStatus->cbiStatusFunction(erropdata_tsetpoint, ( MOSI_frame[DB11] - offset_Troom ));
        }
        break;
      case 0x81:                              // 5 THI-R1 or 6 THI-R2
        if ((MOSI_frame[DB6] & 0x80) != 0) {  // 5 THI-R1
          if ((MOSI_frame[DB10] & 0x30) == 0x20) {
            if (MOSI_frame[DB11] != op_thi_r1_old) {
              op_thi_r1_old = MOSI_frame[DB11];
              m_cbiStatus->cbiStatusFunction(opdata_thi_r1, op_thi_r1_old);
            }
          }
          else
            m_cbiStatus->cbiStatusFunction(erropdata_thi_r1, MOSI_frame[DB11]);
        }
        else {                                // 6 THI-R2
          if (MOSI_type_opdata) {
            if (MOSI_frame[DB11] != op_thi_r2_old) {
              op_thi_r2_old = MOSI_frame[DB11];
              m_cbiStatus->cbiStatusFunction(opdata_thi_r2, op_thi_r2_old);
            }
          }
          else
            m_cbiStatus->cbiStatusFunction(erropdata_thi_r2, MOSI_frame[DB11]);
        }
        break;
      case 0x87:
        if ((MOSI_frame[DB6] & 0x80) != 0) {  // 7 THI-R3
          if (MOSI_type_opdata) {
            if (MOSI_frame[DB11] != op_thi_r3_old) {
              op_thi_r3_old = MOSI_frame[DB11];
              m_cbiStatus->cbiStatusFunction(opdata_thi_r3, op_thi_r3_old);
            }
          }
          else
            m_cbiStatus->cbiStatusFunction(erropdata_thi_r3, MOSI_frame[DB11]);
        }
        break;
      case 0x80:                              // 3 RETURN-AIR or 21 OUTDOOR
        if ((MOSI_frame[DB6] & 0x80) != 0) {  // 3 RETURN-AIR
          if ((MOSI_frame[DB10] & 0x30) == 0x20) {           // operating Data
//##################### Changed Core code ######################################### 20231208
            if ( (MOSI_frame[DB11] != op_return_air_old && IU_sample_count == 0) || tsetpoint_tmp != tsetpoint_tmp_old ) {    // and only, when the IU sampling has been completed
              op_return_air_old = MOSI_frame[DB11];
              tsetpoint_tmp_old = tsetpoint_tmp;
              delta_thermo = (op_return_air_old - offset_Troom) - (tsetpoint_tmp_old * 2);          // leaving out the adding by 61 to be enable to display negative delta and the value has a 0.25 resolution
              m_cbiStatus->cbiStatusFunction(opdata_return_air, op_return_air_old-offset_Troom);    // corrected value leveling it with target temperature
              m_cbiStatus->cbiStatusFunction(opdata_delta_thermostat, delta_thermo );               // publish difference between thermostat and archived temperature
            }
//##################### End changed Core code #####################################
          }
          else 
             m_cbiStatus->cbiStatusFunction(erropdata_return_air, MOSI_frame[DB11]);
        } 
        else {                                // 21 OUTDOOR
          if (MOSI_type_opdata) {
            if (MOSI_frame[DB11] != op_outdoor_old) {
              op_outdoor_old = MOSI_frame[DB11];
              m_cbiStatus->cbiStatusFunction(opdata_outdoor, op_outdoor_old);
            }
          }
          else
            m_cbiStatus->cbiStatusFunction(erropdata_outdoor, MOSI_frame[DB11]);
        }
        break;
      case 0x1f:                              // 8 IU-FANSPEED or 34 OU-FANSPEED
        if ((MOSI_frame[DB6] & 0x80) != 0) {  // 8 IU-FANSPEED
          if (MOSI_type_opdata) {
            if (MOSI_frame[DB10] != op_iu_fanspeed_old) {
              op_iu_fanspeed_old = MOSI_frame[DB10];
              m_cbiStatus->cbiStatusFunction(opdata_iu_fanspeed, op_iu_fanspeed_old & 0x0f);
            }
          }
          else
            m_cbiStatus->cbiStatusFunction(erropdata_iu_fanspeed, MOSI_frame[DB10] & 0x0f);
        }
        else {                                // 34 OU-FANSPEED
          if (MOSI_type_opdata) {
            if (MOSI_frame[DB10] != op_ou_fanspeed_old) {
              op_ou_fanspeed_old = MOSI_frame[DB10];
              m_cbiStatus->cbiStatusFunction(opdata_ou_fanspeed, op_ou_fanspeed_old & 0x0f);
            }
          }
          else
            m_cbiStatus->cbiStatusFunction(erropdata_ou_fanspeed, MOSI_frame[DB10] & 0x0f);
        }
        break;
      case 0x1e:                              // 12 TOTAL-IU-RUN or 37 TOTAL-COMP-RUN
        if ((MOSI_frame[DB6] & 0x80) != 0) {  // 12 TOTAL-IU-RUN
          if (MOSI_type_opdata) {
            if (MOSI_frame[DB11] != op_total_iu_run_old) {
              op_total_iu_run_old = MOSI_frame[DB11];
              m_cbiStatus->cbiStatusFunction(opdata_total_iu_run, op_total_iu_run_old);
            }
          }
          else
            m_cbiStatus->cbiStatusFunction(erropdata_total_iu_run, MOSI_frame[DB11]);
        }
        else {                                // 37 TOTAL-COMP-RUN
          if (MOSI_frame[DB10] == 0x11) {
            if (MOSI_frame[DB11] != op_total_comp_run_old) {
              op_total_comp_run_old = MOSI_frame[DB11];
              m_cbiStatus->cbiStatusFunction(opdata_total_comp_run, op_total_comp_run_old);
            }
          }
          else
            m_cbiStatus->cbiStatusFunction(erropdata_total_comp_run, MOSI_frame[DB11]);
        }
        break;
      case 0x82:
        if ((MOSI_frame[DB6] & 0x80) == 0) {  // 22 ThO-R1
          if (MOSI_type_opdata) {    // operating data
            if (MOSI_frame[DB11] != op_tho_r1_old) {
              op_tho_r1_old = MOSI_frame[DB11];
              m_cbiStatus->cbiStatusFunction(opdata_tho_r1, op_tho_r1_old);
            }
          }
          else
            m_cbiStatus->cbiStatusFunction(erropdata_tho_r1, MOSI_frame[DB11]);
        }
        break;
      case 0x11:
        if ((MOSI_frame[DB6] & 0x80) == 0) {  // 24 COMP
          if (MOSI_type_opdata) {
            if ((MOSI_frame[DB10] << 8 | MOSI_frame[DB11]) != op_comp_old) {
              op_comp_old = MOSI_frame[DB10] << 8 | MOSI_frame[DB11];
              m_cbiStatus->cbiStatusFunction(opdata_comp, op_comp_old & 0x0fff);
            }
          }
          else
            m_cbiStatus->cbiStatusFunction(erropdata_comp, (MOSI_frame[DB10] << 8 | MOSI_frame[DB11]) & 0x0fff);
        }
        break;
      case 0x85:
        if ((MOSI_frame[DB6] & 0x80) == 0) {  // 27 Td
          if (MOSI_type_opdata) {
            if (MOSI_frame[DB11] != op_td_old) {
              op_td_old = MOSI_frame[DB11];
              m_cbiStatus->cbiStatusFunction(opdata_td, op_td_old);
            }
          }
          else
            m_cbiStatus->cbiStatusFunction(erropdata_td, MOSI_frame[DB11]);
        }
        break;
      case 0x90:
        if ((MOSI_frame[DB6] & 0x80) == 0) {  // 29 CT
          if (MOSI_type_opdata) {
            if (MOSI_frame[DB11] != op_ct_old) {
              op_ct_old = MOSI_frame[DB11];
              m_cbiStatus->cbiStatusFunction(opdata_ct, op_ct_old);
            }
          }
          else
            m_cbiStatus->cbiStatusFunction(erropdata_ct, MOSI_frame[DB11]);
        }
        break;
      case 0xb1:
        if ((MOSI_frame[DB6] & 0x80) == 0) {  // 32 TDSH
          if (MOSI_type_opdata) {
            if (MOSI_frame[DB11] != op_tdsh_old) {
              op_tdsh_old = MOSI_frame[DB11];
              m_cbiStatus->cbiStatusFunction(opdata_tdsh, op_tdsh_old / 2);
            }
          }
        }
        break;
      case 0x7c:
        if ((MOSI_frame[DB6] & 0x80) == 0) {  // 33 PROTECTION-No
          if (MOSI_type_opdata) {
            if (MOSI_frame[DB11] != op_protection_no_old) {
              op_protection_no_old = MOSI_frame[DB11];
              m_cbiStatus->cbiStatusFunction(opdata_protection_no, op_protection_no_old);
            }
          }
        }
        break;
      case 0x0c:
        if ((MOSI_frame[DB6] & 0x80) == 0) {  // 36 DEFROST
          if (MOSI_type_opdata) {
            if (MOSI_frame[DB10] != op_defrost_old) {
              op_defrost_old = MOSI_frame[DB10];
              m_cbiStatus->cbiStatusFunction(opdata_defrost, op_defrost_old & 0b1);
            }
          }
        }
        break;
      case 0x13:
        if ((MOSI_frame[DB6] & 0x80) == 0) {  // 38 OU-EEV
          if (MOSI_type_opdata) {
            if ((MOSI_frame[DB12] << 8 | MOSI_frame[DB11]) != op_ou_eev1_old) {
              op_ou_eev1_old = MOSI_frame[DB12] << 8 | MOSI_frame[DB11];
              m_cbiStatus->cbiStatusFunction(opdata_ou_eev1, op_ou_eev1_old);
            }
          }
          else
            m_cbiStatus->cbiStatusFunction(erropdata_ou_eev1, MOSI_frame[DB12] << 8 | MOSI_frame[DB11]);
        }
        break;
      case 0x45: // last error number or count of following error operating data
        if ((MOSI_frame[DB6] & 0x80) != 0) {
          if (MOSI_frame[DB10] == 0x11) {     // last error number
            m_cbiStatus->cbiStatusFunction(erropdata_errorcode, MOSI_frame[DB11]);
          }
          else if (MOSI_frame[DB10] == 0x12) { // count of following error operating data
            erropdataCnt = MOSI_frame[DB11] + 4;
          }
        }
        break;
      case 0x00:  // dummy
        break;
      case 0xff:  // default
        break;
      default:    // unknown operating data
        m_cbiStatus->cbiStatusFunction(opdata_unknown, MOSI_frame[DB10] << 8 | MOSI_frame[DB9]);
        Serial.printf("Unknown operating data, MOSI_frame[DB9]=%i MOSI_frame[D10]=%i\n", MOSI_frame[DB9], MOSI_frame[DB10]);
    }
  }
  return call_counter;      // and loop
}  

