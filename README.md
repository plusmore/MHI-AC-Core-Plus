# MHI-AC-Ctrl-Plus

**This version on GitLab is a plus version that has extra functions and extra output of data. The mhi_ac_ctrl.h code is made to be used with Home Assistant / ESPhome.**

**More specific information about how to use the plus version will be made available as soon I have time to write it up. This version was initially writen to be transparant and it could be used as standard MHI-AC-Ctrl version by absalom muc. However the last version broke that because the the Core will now handle all temperature sensors (internal/external) and if no external temperature sensor is connected it start using only the internal sensor.**

**A important feature is the support set the thermostat between 10 and 30 degrees celcius when in heating mode. This option can be disabled/enabled from outside of MHI-AC-Ctrl Core Plus.**

**Supporting now nativly setting halve degrees and offset to correct the internal/external temperature sensor. There is also a separate correction available for only the internal sensor that allow bigger corrections to compensate for the bad position of the sensor when the the unit is used to heat the room. The correction set is only applied when the unit is set to heating mode.**

Questions & Answers:

Q: why did you develop this plus version?
<details><summary>Click to expand</summary>
mainly for myself, sharing it now with everyone
</details>

Q: why publish it on Gitlab and not on Github where also the original development is hosted?
<details><summary>Click to expand</summary>
my long time aversion for Microsoft, who is the owner of Github makes me not to use that platform
</details>

Q: why is it called Plus?
<details><summary>Click to expand</summary>
first I wanted to have it replace the current Core but later I renamed it to a plus version so you would be more acceptable for the original developer.
</details>

Q: did you contacted the developers to offer your code?
<details><summary>Click to expand</summary>
I was only able to contact one of the developers and got the response that there was no time to test my code. So that was a dead end for me.
</details>

Q: how did you get in business to write software for the MHI airco/heatpump?
<details><summary>Click to expand</summary>
in the end of 2022 I had MHI airco installed to also use it also for heating the room. Because I was not heating before I wanted to also be able to set it below 18 degrees celcius which it was not possible. So I first bought me the hardware interface and had the software installed by the seller. I knew Node Red and there I took my first steps to write the code.
Having to use Home Assistant (HA)/ESPhome to programm the hardware module I also wrote code in YAML to read the Dallas 18x20 temperature sensor directly. I did not want run HA all the time because I had it on a temporary RaspberryPI that normally was used for something else.
So, using YAML and Node Red I still needed to use the next layer and that was in C++. Ok that should be possible to also learn something of that. In the beginning of 2023 I had completed that but is was not really stable.
After the original software was upgraded it was much stable overhere and I started to write it completely into the Core itself.
</details>

Q: why in the Core and not in the other layers of software?
<details><summary>Click to expand</summary>
this because there I sit on top of the what is exchanged between the MHI and software. I can watch what is happening in milliseconds and have the code react to that. The new features are so nativly available to user, independend from which software of coder is being used.
</details>

Q: what is the advantage upgrading the Core to Plus?
<details><summary>Click to expand</summary>
the plus version is written to be transparant to the sofware layers above. So if you use a different sofware to communicate with the Core Plus it should behave the same way a with the Core. 
</details>

Q: do I have to be able to set the temperature below 18 degrees celcius when heating
<details><summary>Click to expand</summary>
no you don't, you can disable this by setting **set_lowtempheating(boolean lowtempheat)** to false
</details>

Q: does the user interface whan it has to display a scale enabling to set the temperature as low as 10 degrees celcius?
<details><summary>Click to expand</summary>
in my example code of mhi_ac_ctrl.h for HA it is dynamicly changed. You have than a different scale when the mode is heating than in the other modes.
</details>

Q: is it possible to also set the MHI to cool below 18 degrees Celcius?
<details><summary>Click to expand</summary>
it is not advised to set to lower temperatures than 18 degrees. So it was not implemented.
</details>

Q: what are those two other files show, beside the two Core-Plus files
<details><summary>Click to expand</summary>
those are the files I use and I trown those in for others to see how I handle things. 
</details>

Q: how does the internal sensor value becomes an external sensor value? 
<details><summary>Click to expand</summary>
that was breaking change to allow this. An external senor can be manipulated, so what if the internal sensor is treated the same way? For five seconds the internal sensor is sampled and as soon three samples are in they are middled and tranfered to new_Troom (external sensor). Now the internal sonsor value has the same options like the external one. The internal sonsor value is still being transmitted and can be used seperate from the generated new_Troom. 
Then, this is not a advisable situation and better is to use an external sensor. This because of location of the internal sonsor, in the inner unit (IU) and measured a value that has to be corrected all the time.
</details>

Q: what do you mean with a "breaking change"?
<details><summary>Click to expand</summary>
I use that for a change that ignores information sent from the outside. Here is the triggering of the measuring the IU-sensor (0xff being received). The breaking change is that now always the internal temperature sensor is sampled and when complete it is transmitted and then replaced by the external temperature sensor value.
If no external temperature value is received then the internal sensor is also used as new_Troom.
</details>

Q: can I switch between 20 frames and the newer 33 frames mode when using a unit that not supports horizontal vanes and 3D?
<details><summary>Click to expand</summary>
you change between legacy and extended mode in the example YAML file for ESPhome/HomeAsssistant by changing the "initial_value" from true to false and then activate legancy.yaml instead of extended.yaml in the lines starting with .common

```
# set the framesize to be used in the Core
  - id: extended_frame_size
    type: bool
    restore_value: no
    initial_value: 'true' # false = 20 frames legacy, true = 33 frames being used - 3Dauto and L/R vanes are available when true
# also choose the matching YAML file underneath by adding and removing the '#' in front the line. Only one line can be activated!
.common: !include &common include/extended.yaml
#.common: !include &common include/legacy.yaml
```

</details>

It seems that an ESP32-C3 is now stable in ESPhome/Home Assistant and you need to modify the MHI-AC-Ctrl interface and loose two pins in the socket of the ESP32-C3.

This because despite the scheme indicating the pins are the same they are not on the C3.

<img src="images/IMG_20231023_100523.jpg" align="left" />


When reverting to an 8266EX then you have remove two pin in the socket of the 8266EX.

<img src="images/IMG_20231023_101009.jpg" align="left" />


_More to come....._

Reads and writes data (e.g. power, mode, fan status etc.) from/to a Mitsubishi Heavy Industries (MHI) air conditioner (AC) via SPI controlled by MQTT. The AC is the SPI master and the ESP8266 is the SPI slave.

<img src="images/webinterface.JPG" width=200 align="right" />

# Attention:
:warning: You have to open the indoor unit to have access to the SPI. Opening of the indoor unit should be done by 
a qualified professional because faulty handling may cause leakage of water, electric shock or fire! :warning: 

# Prerequisites:
For use of the program you have to connect your ESP8266 (I use a LOLIN(WEMOS) D1 R2 & mini with 80 MHz) via a
cable connector to your air conditioner. This has to be a split device (separated indoor and outdoor unit).
I assume that all AC units of the type "SRK xx ZS-S" / "SRC xx ZS-S" are supported. I use the indoor unit SRK 35 ZS-S and the outdoor unit SRC 35 ZS-S. Users reported that additionally the following models are supported:

- [SRF xx ZJX-S1](https://github.com/absalom-muc/MHI-AC-Ctrl/issues/17#issue-632187165)
- [SRF xx ZMX-S](https://github.com/absalom-muc/MHI-AC-Ctrl/issues/17#issuecomment-704789297)
- [SRF xx ZMXA-S](https://github.com/absalom-muc/MHI-AC-Ctrl/pull/91)
- SRF xx ZF-W
- [SRK xx ZJ-S](https://github.com/absalom-muc/MHI-AC-Ctrl/issues/10)
- [SRK xx ZM-S](https://github.com/absalom-muc/MHI-AC-Ctrl/issues/9)
- [SRK xx ZJX-S](https://github.com/absalom-muc/MHI-AC-Ctrl/issues/17#issuecomment-646469621)
- [SRK xx ZJX-S1](https://github.com/absalom-muc/MHI-AC-Ctrl/issues/17#issuecomment-646968940)
- [SRK xx ZRA-W](https://github.com/absalom-muc/MHI-AC-Ctrl/issues/17#issuecomment-730628655)
- [SRK xx ZSA-W](https://github.com/absalom-muc/MHI-AC-Ctrl/issues/17#issuecomment-891649495)
- [SRK xx ZSPR-S](https://github.com/absalom-muc/MHI-AC-Ctrl/issues/149)
- [SRK xx ZSX-S](https://github.com/absalom-muc/MHI-AC-Ctrl/issues/6#issuecomment-582242372)
- [SRK xx ZSX-W](https://github.com/absalom-muc/MHI-AC-Ctrl/issues/17#issuecomment-643748095)
- [SRK xx ZS-W](https://github.com/absalom-muc/MHI-AC-Ctrl/issues/121)
- [SRC 35Z SA-W]

Unsupported models:
- [SRK71ZEA-S1](https://github.com/absalom-muc/MHI-AC-Ctrl/issues/143)
- [SRK35ZC-S](https://github.com/absalom-muc/MHI-AC-Ctrl/issues/154)
 
If you find out that also other models are supported that are not listed here, please give feedback so that I can expand the list. In general, please recheck if your AC has a CNS connector before you spend time and money to build the hardware.

# Installing:

## Hardware:
The ESP8266 is powered from the AC via DC-DC (12V -> 5V) converter. 
The ESP8266 SPI signals SCL (SPI clock), MOSI (Master Out Slave In) and MISO (Master In Slave Out) are connected via a voltage level shifter 5V <-> 3.3V with the AC. Direct connection of the signals without a level shifter could damage your ESP8266!
More details are described in [Hardware.md](Hardware.md).

## Software:
The program uses the following libraries
 - :warning:[MQTT client library](https://github.com/knolleary/pubsubclient) - please don't use v2.8.0! (because of this [issue](https://github.com/knolleary/pubsubclient/issues/747)). Better use v2.7.0:warning:
 - [ArduinoOTA](https://github.com/esp8266/Arduino/tree/master/libraries/ArduinoOTA) (might be removed in future)
 
and optionally you need for the use of an external temperature sensor DS18x20 the libraries
 - [OneWire](https://www.pjrc.com/teensy/td_libs_OneWire.html)
 - [DallasTemperature](https://github.com/milesburton/Arduino-Temperature-Control-Library)

Please check the GitHub pages to see how to install them (usually via tools -> libraries).

Create a sub-directory "esphome" and copy the files from the latest [release](https://github.com/absalom-muc/MHI-AC-Ctrl/releases) src directory in your MHI-AC-Ctrl sub-directory. You could also use the recently updated version in the [src folder](src) but with the risk that it is more unstable. The stability of the program is better when you compile it for a CPU frequency of 160MHz.
The configuration options are described in [SW-Configuration.md](SW-Configuration.md).

In a previous version (see [here](https://github.com/absalom-muc/MHI-AC-SPY)) I used the Hardware-SPI of the ESP8266. But since the SPI documentation of ESP8266 is poor, I decided to switch to a Software based SPI.
This Software based SPI is reliable and the performance of the ESP8266 is sufficient for this use case.
In case of problems please check the [Troubleshooting guide](Troubleshooting.md).

# Enhancement
If you are interested to have a deeper look on the SPI protocol or want to trace the SPI signals, please check [MHI-AC-Trace](https://github.com/absalom-muc/MHI-AC-Trace). But this is not needed for the standard user of MHI-AC-Ctrl-Plus.

# License
This project is licensed under the MIT License - see the LICENSE.md file for details

# Acknowledgments
The coding of the [SPI protocol](https://github.com/absalom-muc/MHI-AC-Trace/blob/main/SPI.md) of the AC is a nightmare. Without [rjdekker's MHI2MQTT](https://github.com/rjdekker/MHI2MQTT) I had no chance to understand the protocol! Unfortunately rjdekker is no longer active on GitHub. He used an Arduino plus an ESP8266 for his project.
Also thank you very much on the authors and contributors of [MQTT client](https://github.com/knolleary/pubsubclient), [ArduinoOTA](https://github.com/esp8266/Arduino/tree/master/libraries/ArduinoOTA), [OneWire](https://www.pjrc.com/teensy/td_libs_OneWire.html) and [DallasTemperature](https://github.com/milesburton/Arduino-Temperature-Control-Library) libraries.

Last but not least thank you for the implementation of MHI-AC-Ctrl in [different environments](https://github.com/absalom-muc/MHI-AC-Ctrl/blob/master/SW-Configuration.md#integration-examples) (FHEM, Tasmota, Home Assistant etc.)
